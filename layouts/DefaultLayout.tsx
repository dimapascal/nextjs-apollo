import React from 'react';
import { Container } from 'next/app';
import HeaderComponent from '../components/HeaderComponent';
import { useMeQuery } from '../graphql/generators';
import { Box } from '@chakra-ui/react';

type DefaultLayoutProps = {
    children?: React.ReactNode | Array<React.ReactNode>;
};
export const DefaultLayout: React.FC<DefaultLayoutProps> = (props) => {
    const { children } = props;
    const { data } = useMeQuery();

    return (
        <Box as="main" minHeight="100vh">
            <HeaderComponent username={data?.me?.name} />
            <Container maxW="xl">{children}</Container>
        </Box>
    );
};
