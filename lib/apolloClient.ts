import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import fetch from 'isomorphic-unfetch';

export const cache = new InMemoryCache();

export default function createApolloClient(initialState: any, ctx: any): any {
    // The `ctx` (NextPageContext) will only be present on the server.
    // use it to extract auth headers (ctx.req) or similar.

    const httpLink = new HttpLink({
        uri: 'http://localhost:4000/graphql', // Server URL (must be absolute)
        fetch,
        headers: {
            cookie: (typeof window === 'undefined' ? ctx?.req?.headers.cookie : undefined) || ''
        }
    });
    const client: any = new ApolloClient({
        ssrMode: Boolean(ctx),
        link: httpLink,
        cache: cache.restore(initialState)
    });
    return client;
}
