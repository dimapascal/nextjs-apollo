import Head from 'next/head';
import React from 'react';
import { ChakraProvider } from '@chakra-ui/react';
import { theme } from '../theme';

const MyApp = (props: any) => {
    const { Component, pageProps } = props;
    return (
        <React.Fragment>
            <Head>
                <meta
                    name="viewport"
                    content="minimum-scale=1, initial-scale=1, width=device-width"
                />
            </Head>
            <ChakraProvider theme={theme}>
                <Component {...pageProps} />
            </ChakraProvider>
        </React.Fragment>
    );
};

export default MyApp;
