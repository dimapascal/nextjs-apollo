import { Flex, Text } from '@chakra-ui/react';
import React from 'react';
import { DefaultLayout } from '../layouts/DefaultLayout';
import { withApollo } from '../lib/apollo';

const NotFoundPage: React.FC = () => {
    return (
        <DefaultLayout>
            <Flex w="100%" h="100%" align="center" justify="center" direction="column">
                <Text fontSize="50px" color="tomato">
                    Page not found
                </Text>
                <Text fontSize="60px" color="tomato">
                    404
                </Text>
            </Flex>
        </DefaultLayout>
    );
};

export default withApollo({ ssr: false })(NotFoundPage);
