import React from 'react';
import { withApollo } from '../lib/apollo';
import { DefaultLayout } from '../layouts/DefaultLayout';
import { useUsersQuery } from '../graphql/generators';
import { Box } from '@chakra-ui/react';

const HomePage = () => {
    const { data } = useUsersQuery();
    return (
        <DefaultLayout>
            <Box as="pre" w="100%" minHeight="100vh">
                {JSON.stringify(data?.users, null, 2)}
            </Box>
        </DefaultLayout>
    );
};

export default withApollo({ ssr: true })(HomePage);
