import React from 'react';
import { Button, ButtonProps } from '@chakra-ui/react';
import NextLink, { LinkProps } from 'next/link';

type LinkComponentProps = {
    uiProps?: ButtonProps;
};

const LinkComponent = ({
    uiProps,
    children,
    ...props
}: React.PropsWithChildren<LinkProps & LinkComponentProps>) => {
    return (
        <NextLink {...props}>
            <Button as="a" {...uiProps}>
                {children}
            </Button>
        </NextLink>
    );
};

export default LinkComponent;
