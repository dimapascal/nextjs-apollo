import { Box, Flex, Spacer } from '@chakra-ui/react';
import LinkComponent from '../LinkComponent';
type HeaderComponentProps = {
    username?: string;
};

const linkComponentUiProps = {
    colorScheme: 'white'
};

const HeaderComponent = ({ username }: HeaderComponentProps) => {
    let endLinksComponents = null;

    if (username)
        endLinksComponents = (
            <>
                <LinkComponent uiProps={linkComponentUiProps} href="/account/me">
                    User: {username}
                </LinkComponent>
            </>
        );
    else
        endLinksComponents = (
            <>
                <LinkComponent uiProps={linkComponentUiProps} href="/login">
                    LogIn
                </LinkComponent>
                <LinkComponent uiProps={linkComponentUiProps} href="/register">
                    Register
                </LinkComponent>
            </>
        );

    return (
        <Box bg="tomato" w="100%" p={4} color="white">
            <Flex>
                <LinkComponent uiProps={linkComponentUiProps} href="/">
                    Home
                </LinkComponent>
                <Spacer />
                {endLinksComponents}
            </Flex>
        </Box>
    );
};

export default HeaderComponent;
